﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" Runat="Server">
    <title>Register</title>
    <link rel="stylesheet" type="text/css" href="/res/css/userAction.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Registar</h1>
    <form runat="server" id="registerForm">
        <asp:TextBox ID="email_txtBox" runat="server" CssClass="txtBox" placeholder="Endereço Eletronico"></asp:TextBox>
        <asp:TextBox ID="name_txtBox" runat="server" CssClass="txtBox" placeholder="Nome de utilizador"></asp:TextBox>
        <asp:TextBox ID="password_txtBox" runat="server" CssClass="txtBox" placeholder="Palavra-Passe" TextMode="Password"></asp:TextBox>
        <asp:TextBox ID="confirm_txtBox" runat="server" CssClass="txtBox" placeholder="Confirmar Palavra-Passe" TextMode="Password"></asp:TextBox>
        <asp:Button ID="register_btn" runat="server" CssClass="submitBtn" Text="Submeter" OnClick="register_btn_Click" />
    </form>
    <button onclick="location.href = '/login.aspx'" class="redirectBtn">Ja tem uma conta?</button>
</asp:Content>

