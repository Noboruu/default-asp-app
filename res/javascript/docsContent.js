﻿var overContent = document.getElementById('overview_content');
var dbContent = document.getElementById('database_content');
var dbCreateContent = document.getElementById('db_create_content');
var dbConnectContent = document.getElementById('db_connect_content');
var overviewBtn = document.getElementById('overview');
var dbBtn = document.getElementById('database');
var dbCreateBtn = document.getElementById('db_create');
var dbConnectBtn = document.getElementById('db_connect');

function getOverview() {
    dbContent.style.display = 'none';
    dbCreateContent.style.display = 'none';
    dbConnectContent.style.display = 'none';

    overContent.style.display = 'inline-block';
    overviewBtn.className = "choice_btn activeTree_btn";
    dbBtn.className = "choice_btn";
    dbCreateBtn.className = "choice_btn choiceChild_btn";
    dbConnectBtn.className = "choice_btn choiceChild_btn";
}

function getDatabase() {
    dbContent.style.display = 'inline-block';
    dbCreateContent.style.display = 'none';
    dbConnectContent.style.display = 'none';

    overContent.style.display = 'none';
    overviewBtn.className = "choice_btn";
    dbBtn.className = "choice_btn activeTree_btn";
    dbCreateBtn.className = "choice_btn choiceChild_btn";
    dbConnectBtn.className = "choice_btn choiceChild_btn";
}

function getDbCreate() {
    dbContent.style.display = 'none';
    dbCreateContent.style.display = 'inline-block';
    dbConnectContent.style.display = 'none';

    overContent.style.display = 'none';
    overviewBtn.className = "choice_btn";
    dbBtn.className = "choice_btn";
    dbCreateBtn.className = "choice_btn choiceChild_btn activeTree_btn";
    dbConnectBtn.className = "choice_btn choiceChild_btn";
}

function getDbConnect() {
    dbContent.style.display = 'none';
    dbCreateContent.style.display = 'none';
    dbConnectContent.style.display = 'inline-block';

    overContent.style.display = 'none';
    overviewBtn.className = "choice_btn";
    dbBtn.className = "choice_btn";
    dbCreateBtn.className = "choice_btn choiceChild_btn";
    dbConnectBtn.className = "choice_btn choiceChild_btn activeTree_btn";
}