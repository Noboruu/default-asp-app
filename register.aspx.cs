﻿using System;
using database;
using website;

public partial class register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void register_btn_Click(object sender, EventArgs e)
    {
        //These checks should be done in client side before sending it to the server!
        if ((email_txtBox.Text != "") && (password_txtBox.Text != "") && (name_txtBox.Text != "") && (confirm_txtBox.Text != "")) //Checks if the variables are set
        {
            if (password_txtBox.Text == confirm_txtBox.Text)
            {
                String password = md5.GetHashString(password_txtBox.Text);
                BLL.users.register(email_txtBox.Text, name_txtBox.Text, password);
            }
        }
    }
}