﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="Server">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="/res/css/userAction.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>Iniciar Sessão</h1>
    <form runat="server" id="loginForm">
        <asp:TextBox ID="username_txtBox" runat="server" CssClass="txtBox" placeholder="Nome de Utilizador"></asp:TextBox>
        <asp:TextBox ID="password_txtBox" runat="server" CssClass="txtBox" TextMode="Password" placeholder="Palavra-Passe"></asp:TextBox>
        <asp:Button  ID="login_btn" runat="server" Text="Iniciar Sessão" CssClass="submitBtn" OnClick="loginbtn_Click" />
    </form>
    <button onclick="location.href = '/register.aspx'" class="redirectBtn">Registar</button>
</asp:Content>

