﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" Runat="Server">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="/res/css/default.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>Bem vindo ao teu novo lindo site!</h1>
    <p>Este vai ser o principio do teu site.</p>
    <p>Regista-te ou inicia sessão para começares!</p>
    <p>Se não tiveres uma base de dados ou se não saberes aonde ver se ja tens uma seleciona o butão de tutorial na "nav-bar".</p>
    <p>Após efetuares o inicio de sessão esta pagina vai conter mais passos para ajudar-te a criares a tua primeira pagina web!</p>
</asp:Content>

