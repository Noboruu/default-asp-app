﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace website
{
    public class md5
    {
        //Note: This method shouldn't be used in production. Always try to encrypt in the client side (javascript) and then send it to the server.
        //For extra security you should always use SSL/TLS.
        //For more info on this check: https://www.sslshopper.com/why-ssl-the-purpose-of-using-ssl-certificates.html
        //And: https://luxsci.com/blog/ssl-versus-tls-whats-the-difference.html
        //And you can always google for some more info ;)
        public static byte[] GetHash(String inputString) //This creates the MD5 hash to keep the password secure
        {
            HashAlgorithm algorithm = MD5.Create();  //You can also use SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString) //Returns the Byte array hash as a normal String
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}