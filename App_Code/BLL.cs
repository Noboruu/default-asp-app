﻿using System;
using DataAccessLayer;
using System.Data;
using System.Data.SqlClient;


namespace database
{
    public class BLL
    {
        public class users
        {
            static public int register(String email, String username, String password) { 
                DAL dal = new DAL();
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@email", email),
                    new SqlParameter("@username", username),
                    new SqlParameter("@password", password),
                };

                return dal.executarNonQuery("INSERT into users (email, username, password) VALUES (@email, @username, @password)", sqlParams);
            }

            static public DataTable login(String username, String password)
            {
                DAL dal = new DAL();
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@username", username),
                    new SqlParameter("@password", password),
                };

                return dal.executarReader("SELECT * FROM users WHERE ((username=@username) and (password=@password))", sqlParams);
            }

            static public int update(String email, String username, String password)
            {
                DAL dal = new DAL();
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@email", email),
                    new SqlParameter("@username", username),
                    new SqlParameter("@password", password),

                };

                return dal.executarNonQuery("UPDATE [users] set email = @email, username = @username, password = @password", sqlParams);
            }
        }
    }
}
