﻿using System;
using website;
using database;
using System.Data;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void loginbtn_Click(object sender, EventArgs e)
    {
        if((username_txtBox.Text != "") && (password_txtBox.Text != ""))
        {
            String password = md5.GetHashString(password_txtBox.Text);
            DataTable user =  BLL.users.login(username_txtBox.Text, password);

            if (user.Rows.Count > 0)
            {
                Session["user"] = username_txtBox.Text; //$_SESSION['user'] == username_txtBox.Text; -> PHP version, ignore this, this way its easier for me :P
                Response.Redirect("/Default.aspx");
            }
        }
    }
}