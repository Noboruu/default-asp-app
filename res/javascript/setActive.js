﻿function setactive() {
    var homeBtn = document.getElementById('homeBtn');
    var loginBtn = document.getElementById('loginBtn');
    var registerBtn = document.getElementById('registerBtn');
    var docsBtn = document.getElementById('tutorialBtn');
    var aboutBtn = document.getElementById('aboutBtn');
    var title = document.title;

    if (title === 'Home') {
        homeBtn.className = "active";
        homeBtn.href = "#";
    } else if (title === 'Login') {
        loginBtn.className = "active";
        loginBtn.href = "#";
    } else if (title === 'Register') {
        registerBtn.className = "active";
        registerBtn.href = "#";
    } else if (title === 'Documentação') {
        docsBtn.className = "active";
        docsBtn.href = "#";
    } else if (title === 'Sobre') {
        aboutBtn.className = "active";
        aboutBtn.href = "#";
    }
}