﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="tutorial.aspx.cs" Inherits="tutorial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="Server">
    <title>Documentação</title>
    <link rel="stylesheet" type="text/css" href="res/css/tutorial.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="sidebar">
        <ul class="choice_tree">
            <li><a id="overview" class="choice_btn activeTree_btn" onclick="getOverview()">Visão Geral</a></li>
            <li><a id="database" class="choice_btn" onclick="getDatabase()">Bases de Dados</a></li>
            <li><a id="db_create" class="choice_btn choiceChild_btn" onclick="getDbCreate()">Criação de uma base de dados</a></li>
            <li><a id="db_connect" class="choice_btn choiceChild_btn" onclick="getDbConnect()">Conexão a uma base de dados</a></li>

            <% if ((Session["user"]) != null)
                { %>

            <% } %>
        </ul>
    </div>

    <div class="docs_content">
        <div id="overview_content">
            <h1>Visão Geral</h1>
            <h2>Bem vindo à documentação de ASP.net!</h2>
            <h3 style="text-align: left;">O que é o ASP.net?</h3>
            <p style="text-align: left; margin-left: 100px;">O ASP.net é uma plataforma da Microsoft para desenvolvimento de páginas web. Este utiliza uma Stack de desenvolvimento que inclui um webserver IIS, .NET framework para codigo server-side em c# e uma base de dados SQL-Server (Microsoft SQL-Server/Transact-SQL).</p>
        </div>

        <div id="database_content" style="display: none;">
            <h1>Base de dados</h1>
            <p>Test</p>
        </div>

        <div id="db_create_content" style="display: none;">
            <h1>Criação de uma base de dados</h1>
        </div>

        <div id="db_connect_content" style="display: none;">
            <h1>Conexão a uma base de dados</h1>
        </div>

    </div>
    <script type="text/javascript" src="res/javascript/docsContent.js"></script>
</asp:Content>

